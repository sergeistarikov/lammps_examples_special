units		metal
atom_style	atomic

boundary	s p s

neighbor	0.3 bin
neigh_modify	 every 2 delay 10 check yes

# a is lattice parameter
# T is temperature

variable	a equal 3.303 #!!!# if a0 = lattice constant at T = 0 (see publication of potential or NIST data): a = 1.0018*a0
variable	T equal 200

variable	d_oy equal 23 #!!!# see results of GB_bcc_energy script
variable	d_oz equal 0  #!!!# 0 or 1, see results of GB_bcc_energy script

#####

# grain 1 (tilt)

variable	x_1 equal 7 #!!!# 4 posible orientations: (210), (310), (510), (730)
variable	x_2 equal 3 #!!!#
variable	x_3 equal 0 

#############################################

# grain 1

variable	y_1 equal -1*${x_2}
variable	y_2 equal ${x_1}
variable	y_3 equal 0
variable	z_1 equal 0
variable	z_2 equal 0
variable	z_3 equal 1

# grain 2

variable	xx_1 equal ${x_1}
variable	xx_2 equal -1*${x_2}
variable	xx_3 equal 0
variable	yy_1 equal -1*${xx_2}
variable	yy_2 equal ${xx_1}
variable	yy_3 equal 0
variable	zz_1 equal 0
variable	zz_2 equal 0
variable	zz_3 equal 1

variable	shift_1 equal 0.05  
variable	shift_2 equal 0.0  
variable	shift_3 equal 0.0  

lattice		bcc $a orient x ${x_1} ${x_2} ${x_3} orient y ${y_1} ${y_2} ${y_3} orient z ${z_1} ${z_2} ${z_3}

# lx ly lz - size of calculation cell

if "${x_1} == 2" then "variable a_y equal 19.9999" 
if "${x_1} == 3" then "variable a_y equal 19.9999" 
if "${x_1} == 5" then "variable a_y equal 17.3333" 
if "${x_1} == 7" then "variable a_y equal 17.3999" 

variable	lx equal 40
variable	ly equal ${a_y}
variable	lz equal 10.45

variable	lx_f equal ${lx}+0.05
variable	l_gb equal -0.02+0.5*${lx}

region		box block -0.05 ${lx_f} 0 ${ly} 0 ${lz}
create_box	1 box      #!!!# in case of binary alloy: should be "2"
create_atoms	1 box

variable	T_in equal 2.0*${T}

mass		1 92.90638 #!!!# mass of element

region		grain block ${l_gb} 1000 -1000 1000 -1000 1000
delete_atoms	region grain

lattice		bcc $a origin ${shift_1} ${shift_2} ${shift_3} orient x ${xx_1} ${xx_2} ${xx_3} orient y ${yy_1} ${yy_2} ${yy_3} orient z ${zz_1} ${zz_2} ${zz_3} 
create_atoms	1 region grain

lattice		bcc $a orient x ${x_1} ${x_2} ${x_3} orient y ${y_1} ${y_2} ${y_3} orient z ${z_1} ${z_2} ${z_3}

group		grain region grain

#group		solv region box				#!!!# only for binary alloys
#set		group solv type/fraction 2 0.15 2639	#!!!# only for binary alloys

group		Nb type 1    #!!!# may be changed: Nb, Fe, Cr, Mo etc.
#group		Cr type 2

variable	x_r equal ${lx}-2.9

velocity	all create ${T_in} 87287
timestep	0.0005

pair_style	adp 
pair_coeff	* * ./Zr_Nb.2021.9.0.final.adp.txt Nb  #!!!# interatomic potential

thermo		100
thermo_style 	custom step temp pe ke etotal pxx pyy pzz pxy pxz pyz press vol 

region		mark block -10000 10000 4 6 -1000 1000
group		mark region mark
compute		pe_mark mark pe/atom
compute		pe_at_0 all pe/atom
compute		disp all displace/atom

variable	r_cut equal ${a}*1.18
compute		cna all cna/atom ${r_cut}

dump		snap0 all cfg 40000 ./dump/dump0bcc.*.cfg mass type xs ys zs type c_pe_at_0 c_disp[4] c_pe_mark c_cna
dump_modify	snap0 element Fe   #!!!# may be changed: Fe, Nb, Cr, Mo etc.

region		m_block block -10000 10000 -10000 10000 2.15 7.15 
group		m_block region m_block

###################

variable	dis_oy equal 0.05*${d_oy}
variable	dis_oz equal 0.43*${d_oz}

displace_atoms	grain move 0.0 ${dis_oy} ${dis_oz} units lattice

##################

min_style	hftn
minimize	1.0e-10 1.0e-10 1000 1000

thermo		100

compute		pe_at m_block pe/atom
compute		stress m_block stress/atom NULL 

compute         prof m_block chunk/atom bin/1d x center 1.0
compute         prof_1 m_block chunk/atom bin/1d x center 3.0
compute         prof_2 m_block chunk/atom bin/1d x center 6.0
fix             pr_av m_block ave/chunk 100 200 20000 prof density/number c_pe_at c_stress[1] c_stress[2] c_stress[3] norm sample file ./distr_0.txt
fix             pr_av_1 m_block ave/chunk 100 200 20000 prof_1 density/number c_pe_at c_stress[1] c_stress[2] c_stress[3] norm sample file ./distr_1.txt
fix             pr_av_2 m_block ave/chunk 100 200 20000 prof_2 density/number c_pe_at c_stress[1] c_stress[2] c_stress[3] norm sample file ./distr_2.txt

region		sim_block block 3 36 -10000 10000 -10000 100000
group		sim_block region sim_block

fix		1 all npt y 0.0 0.0 1.0 temp ${T} ${T} 1.0
run		100000
unfix		1

fix 		12 all nve
run		5000
velocity	all create ${T} 85287
run		5000
velocity	all create ${T} 85284
run		5000
velocity	all create ${T} 85187
run		5000
velocity	all create ${T} 85087
run		5000
unfix		12

fix		2 sim_block nvt temp 200 2500 2.0

run		6000000

